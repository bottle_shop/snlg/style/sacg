import os
import tensorflow as tf

class CNN_Model(object):

    def __init__(self, args, vocab, scope=''):
        # scope is the parent scope outside of the CNN
        with tf.variable_scope("classifer"):
            dim_emb = args.dim_emb
            self.max_len = args.max_len
            self.args = args
            filter_sizes = [int(x) for x in args.filter_sizes.split(',')]
            n_filters = args.n_filters

            self.learning_rate = args.learning_rate
            self.dropout = tf.placeholder(tf.float32,
                name='dropout')
            self.input = tf.placeholder(tf.int32, [None, None],    #batch_size * max_len
                name='input')
            self.input_graph = tf.placeholder(tf.float32, [None, None, None],
                name='input_graph')
            self.enc_lens = tf.placeholder(tf.int32, [None],
                name='enc_lens')
            self.label = tf.placeholder(tf.int32, [None],
                name='label')
            self.global_step = tf.Variable(0, name='global_step', trainable=False)

            embedding = tf.get_variable('embedding', [vocab.size, dim_emb])
            x = tf.nn.embedding_lookup(embedding, self.input)

            batch_len = tf.shape(x)[1]
            mask = tf.expand_dims(tf.sequence_mask(self.enc_lens, batch_len, tf.float32), -1)
            x *= mask
            
            self.logits, self.feat = self.cnn(x, self.enc_lens, self.input_graph, filter_sizes, n_filters, self.dropout, 'cnn')

            self.probs = tf.nn.softmax(self.logits, -1)
            self.preds = tf.argmax(self.probs, -1, output_type=tf.int32)
            self.correct_preds = tf.equal(self.preds, self.label)

            loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=self.label, logits=self.logits)
            self.loss = tf.reduce_mean(loss)

        # optimizer
        self.optimizer = tf.train.AdamOptimizer(self.learning_rate)
        self.train_op = self.optimizer.minimize(self.loss, self.global_step)
        self.params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=os.path.join(scope, 'classifer'))
        self.saver = tf.train.Saver(self.params)

    def leaky_relu(self, x, alpha=0.01):
        return tf.maximum(alpha * x, x)

    def cnn(self, inp, sen_len, inp_graph, filter_sizes, n_filters, dropout, scope, reuse=False):
        cell_num = self.args.cell_num
        layer_num = self.args.lstm_layer_num
        dim = inp.get_shape().as_list()[-1]
        inp = tf.expand_dims(inp, -1)

        with tf.variable_scope(scope) as vs:
            if reuse:
                vs.reuse_variables()

#             outputs = []
#             for size in filter_sizes:
#                 with tf.variable_scope('conv-maxpool-%s' % size):
#                     W = tf.get_variable('W', [size, dim, 1, n_filters])
#                     b = tf.get_variable('b', [n_filters])
#                     conv = tf.nn.conv2d(inp, W,
#                         strides=[1, 1, 1, 1], padding='VALID')
#                     h = self.leaky_relu(conv + b)
#                     # max pooling over time
#                     pooled = tf.reduce_max(h, reduction_indices=1)
#                     pooled = tf.reshape(pooled, [-1, n_filters])
#                     outputs.append(pooled)
#             outputs = tf.concat(outputs, 1)
#             outputs = tf.nn.dropout(outputs, dropout)
            lstm_feat, lstm_output = self.bi_lstm(tf.squeeze(inp, [-1]), sen_len, cell_num, layer_num)
            graph_feat = self.gcn(tf.reshape(lstm_feat, [-1, cell_num*2]), inp_graph, cell_num*2, cell_num*2, n_filters*len(filter_sizes), dropout)
#             outputs = tf.concat([outputs, graph_feat], 1)
            outputs = tf.concat([lstm_output, graph_feat], 1)

            with tf.variable_scope('output'):
                W = tf.get_variable('W', [n_filters*len(filter_sizes)+cell_num*2, 2])
                b = tf.get_variable('b', [2])
                logits = tf.matmul(outputs, W) + b

        return logits, outputs
    
    def scaled_dot_product_attention(self, q, k, v, mask):
        matmul_qk = tf.matmul(q, k, transpose_b=True)  # (..., seq_len_q, seq_len_k)

        # 缩放 matmul_qk
        dk = tf.cast(tf.shape(k)[-1], tf.float32)
        scaled_attention_logits = matmul_qk / tf.math.sqrt(dk)

        # 将 mask 加入到缩放的张量上。
        if mask is not None:
            scaled_attention_logits += (mask * -1e9)  

        # softmax 在最后一个轴（seq_len_k）上归一化，因此分数
        # 相加等于1。
        attention_weights = tf.nn.softmax(scaled_attention_logits, axis=-1)  # (..., seq_len_q, seq_len_k)

        output = tf.matmul(attention_weights, v)  # (..., seq_len_q, depth_v)
        output = tf.reduce_mean(output, axis=1)
        

        return output, attention_weights  
    
    
    def bi_lstm(self, inp, sen_len, cell_num, layer_num):
        stacked_rnn = []
        stacked_bw_rnn = []
        for i in range(layer_num):
            stacked_rnn.append(tf.contrib.rnn.GRUCell(cell_num))
            stacked_bw_rnn.append(tf.contrib.rnn.GRUCell(cell_num))
            
        mcell = tf.contrib.rnn.MultiRNNCell(stacked_rnn)
        mcell_bw = tf.contrib.rnn.MultiRNNCell(stacked_bw_rnn)
        
        bioutputs, output_state = tf.nn.bidirectional_dynamic_rnn(mcell, mcell_bw, inp,                                                                                     sequence_length=sen_len, dtype=tf.float32)
        lstm_feat = tf.concat([bioutputs[0], bioutputs[1]], 2)
        lstm_output = tf.concat([output_state[0][0], output_state[1][0]], 1)
        return lstm_feat, lstm_output
    
    def gcn(self, input, adj, in_feat_dim, nhid, out_feat_dim, dropout, bias=True):
        output_1 = self.gcn_layer(input, adj, in_feat_dim, nhid, layer_num=1, bias=True)
        output_1 = tf.reshape(tf.nn.relu(output_1), [-1, nhid])
        output_1 = tf.nn.dropout(output_1, dropout)
        #layer2
#         output_2 = self.gcn_layer(output_1, adj, nhid, nhid, layer_num=2, bias=True)
#         output_2 = tf.reshape(tf.nn.relu(output_2), [-1, nhid])
#         output_2 = tf.nn.dropout(output_2, dropout)
        
        output = self.gcn_layer(output_1, adj, nhid, out_feat_dim, layer_num=2, bias=True)
#         output = tf.reduce_mean(output, axis=1)
        output, attn_weight = self.scaled_dot_product_attention(output, output, output, None)
        
        return output
    
    def gcn_layer(self, input, adj, in_features, out_features, layer_num, bias=True):
        with tf.variable_scope('gcn_layer_%s'%layer_num):
            W = tf.get_variable('w', [in_features, out_features])
            if bias:
                b = tf.get_variable('b', [out_features])
        
        support = tf.matmul(input, W)
        support = tf.reshape(support, [-1, self.max_len, out_features])
        output = tf.matmul(adj, support)
        if bias:
            return output + b
        else:
            return output

    def _make_train_feed_dict(self, batch):
        feed_dict = {}
        feed_dict[self.input] = batch.enc_batch
        feed_dict[self.label] = batch.labels
        feed_dict[self.enc_lens] = batch.enc_lens
        feed_dict[self.input_graph] = batch.graph
        feed_dict[self.dropout] = 0.5
        return feed_dict

    def _make_test_feed_dict(self, batch):
        feed_dict = {}
        feed_dict[self.input] = batch.enc_batch
        feed_dict[self.input_graph] = batch.graph
        feed_dict[self.label] = batch.labels
        feed_dict[self.enc_lens] = batch.enc_lens
        feed_dict[self.dropout] = 1.0
        return feed_dict

    def run_train_step(self, sess, batch):
        feed_dict = self._make_train_feed_dict(batch)
        to_return = {
            'train_op': self.train_op,
            'loss': self.loss,
            'global_step': self.global_step,
        }
        return sess.run(to_return, feed_dict)

    def run_eval(self, sess, batches):
        """Runs one evaluation iteration. Returns a dictionary containing summaries, loss, global_step and (optionally) coverage loss."""
        correct = 0
        total = 0
        error_list =[]
        error_label = []
        preds = []
        feat = []
        for batch in batches:
            feed_dict = self._make_test_feed_dict(batch)
            to_return = {
                'predictions': self.correct_preds,
                'pred_confs': self.probs,
                'preds': self.preds,
                'feat': self.feat
            }
            results = sess.run(to_return, feed_dict)

            for i in range(len(results['predictions'])):
                total += 1
                if results['predictions'][i]:
                    correct +=1
                else:
                    error_label.append(results['predictions'][i])
                    error_list.append(batch.original_reviews[i])
            #return predictions
            for i in range(len(results['preds'])):
                preds.append(results['preds'][i])
            #return features
            for i in range(len(results['feat'])):
                feat.append(results['feat'][i].tolist())

        return correct/total, error_list, error_label, preds, feat

    def run_eval_conf(self, sess, batch):
        """Runs one training iteration. Returns a dictionary containing train op, summaries, loss, global_step and (optionally) coverage loss."""
        feed_dict = self._make_test_feed_dict(batch)
        to_return = {
            'pred_conf': self.probs,
        }
        return sess.run(to_return, feed_dict)
