## Welcome

This is the code implementation of our RANLP 2021 paper "Syntax Matters! Syntax-Controlled in Text Style Transfer" 

### Dataset 

**yelp**: is a corpus of restaurant reviews from Yelp collected in https://github.com/shentianxiao/language-style-transfer. The original Yelp reviews are on a 5 points rating scale. As part of data preprocessing, reviews with 3 points and above ratings are labeled as positive, while those below 3 points are labeled as negative. The reviews with an exact 3 points rating considered neutral and are excluded in this dataset.

*ps: we have preprocessed yelp dataset for all the models. We use the whole testset but some models only use a subset for testing.*

**GYAFC**: Since the GYAFC dataset is only free of charge for research purposes, we only publish the ouputs of our model in the family and relationships domain. If you want to download the train and validation dataset, please follow the guidance at https://github.com/raosudha89/GYAFC-corpus. And then, name the corpora of two styles as the yelp dataset.

### Dependency
python >= 3.6
tensorflow >= 1.12
stanza >= 1.0.1

### Run
1. train style classifier
   ```
   CUDA_VISIBLE_DEVICES=0 python train_classifier.py --dataset yelp
   ```

2. train SACG 
   ```
   CUDA_VISIBLE_DEVICES=2 python train_style_transfer.py --dataset yelp --network SACG
   ```

### contact
If you have any question, please send an email to hzq950419@gmail.com
